# ttgo-t-beam-test

Compile with

```
platformio run -e tbeam
```

Compile and upload to board with

```
platformio run -e tbeam -t upload
```