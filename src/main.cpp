#include "Arduino.h"

#include <Wire.h>
#include <TinyGPS++.h>
#include <axp20x.h>
#include <LoRa.h>
#include "SSD1306.h"

#define I2C_SDA         21
#define I2C_SCL         22

#define GPS_SERIAL_NUM  1
#define GPS_BAUDRATE    9600
#define USE_GPS         1
#define GPS_RX_PIN      34
#define GPS_TX_PIN      12

#define SCK 5 // GPIO5 -- SX1278's SCK
#define MISO 19 // GPIO19 -- SX1278's MISnO
#define MOSI 27 // GPIO27 -- SX1278's MOSI
#define SS 18 // GPIO18 -- SX1278's CS
#define RST 23 // GPIO14 -- SX1278's RESET
#define DI0 26 // GPIO26 -- SX1278's IRQ(Interrupt Request)
#define BAND 868E6

AXP20X_Class axp;

SSD1306 display(0x3c, I2C_SDA, I2C_SCL);

String lines[5] = {"", "", "", "", ""};

void display_println(String message)
{
  //update lines
  lines[4] = lines[3];
  lines[3] = lines[2];
  lines[2] = lines[1];
  lines[1] = lines[0];
  lines[0] = message;

  //print lines
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  display.drawString(0, 0, lines[4]);
  display.drawString(0, 12, lines[3]);
  display.drawString(0, 24, lines[2]);
  display.drawString(0, 36, lines[1]);
  display.drawString(0, 48, lines[0]);
  display.display();
}

void print_message(String message)
{
    display.clear();
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_10);
    display.drawString(0, 0, message);
    display.drawString(0, 15, message);
    display.drawString(0, 26, message);
    display.display();
}

#if defined(LORA_SENDER)
void lora_setup ()
{
  SPI.begin(SCK, MISO, MOSI, SS);
  LoRa.setPins(SS, RST, DI0);

  if (!LoRa.begin(BAND)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }

  LoRa.setTxPower(10);

  delay(1000);
}

void lora_send_string(String str)
{
  LoRa.beginPacket();
  LoRa.print(str);
  LoRa.endPacket();
}

void story()
{
  lora_send_string("Line 1");
  delay(5000);
  lora_send_string("Line 2");
  delay(5000);
  lora_send_string("Line 3");
  delay(100);
  lora_send_string("Line 4");
  delay(100);
  lora_send_string("Line 5");
  delay(5000);
  lora_send_string("Line 6");
  delay(100);
  lora_send_string("Line 7");
  delay(5000);
  lora_send_string("Line 8");
  delay(5000);
  lora_send_string("Line 9");
  delay(5000);
}
#elif defined(LORA_RECEIVER)

void rec_lora_string(int packetSize)
{
  String rec_str;

  while (LoRa.available()) {
    rec_str = LoRa.readString();
    Serial.println(rec_str);
    display_println(rec_str);
  }
}

void lora_setup()
{
  SPI.begin(SCK, MISO, MOSI, SS);
  LoRa.setPins(SS, RST, DI0);

  if (!LoRa.begin(BAND)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }

  LoRa.receive();

  delay(1000);
}

void lora_loop()
{
  int packet_size = LoRa.parsePacket();

  if (packet_size) {
    Serial.println("Received Packet!");
    rec_lora_string(packet_size);
  }
  delay(10);
}
#else
#error "Define either LORA_SENDER or LORA_RECEIVER!"
#endif

void pmu_setup()
{
  if (!axp.begin(Wire, AXP192_SLAVE_ADDRESS)) {
    Serial.println("AXP192 Begin PASS");
  } else {
    Serial.println("AXP192 Begin FAIL");
  }
  // axp.setChgLEDMode(LED_BLINK_4HZ);
  Serial.printf("DCDC1: %s\n\r", axp.isDCDC1Enable() ? "ENABLE" : "DISABLE");
  Serial.printf("DCDC2: %s\n\r", axp.isDCDC2Enable() ? "ENABLE" : "DISABLE");
  Serial.printf("LDO2: %s\n\r", axp.isLDO2Enable() ? "ENABLE" : "DISABLE");
  Serial.printf("LDO3: %s\n\r", axp.isLDO3Enable() ? "ENABLE" : "DISABLE");
  Serial.printf("DCDC3: %s\n\r", axp.isDCDC3Enable() ? "ENABLE" : "DISABLE");
  Serial.printf("Exten: %s\n\r", axp.isExtenEnable() ? "ENABLE" : "DISABLE");
  Serial.println("----------------------------------------");

  axp.setPowerOutPut(AXP192_LDO3, AXP202_ON); // GPS main power

  Serial.printf("LDO3: %s\n\r", axp.isLDO3Enable() ? "ENABLE" : "DISABLE");

  delay(50);
}

void display_setup()
{
  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
}

void setup()
{
  Serial.begin(115200);
  Serial.println("Starting up...");

  Wire.begin(I2C_SDA, I2C_SCL);

  pmu_setup();

  lora_setup();

  display_setup();

#if defined(LORA_SENDER)
  story();
  for (;;) ;
#endif

  //serial_gps.begin(GPS_BAUDRATE, SERIAL_8N1, GPS_RX_PIN, GPS_TX_PIN);
}

void loop()
{
  for (;;) {

#if defined(LORA_RECEIVER)
    lora_loop();
#endif

  }
}
